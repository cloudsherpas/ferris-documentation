OAuth2
======

Ferris provides utilities for interacting with Google's implementation of OAuth2, giving the developer easy ways to initiate web server flow, get user credentials, and utilize service accounts.

This documentation assumes you have an understanding of OAuth2. If you'd like to read more on the subject of OAuth2 and how Google uses it please read `Google's documentation <https://developers.google.com/accounts/docs/OAuth2>`_.

.. module:: ferris.core.oauth2


Configuration
-------------

In order to use Ferris' OAuth2 features, you must first configure client settings in ``app/settings.py`` or use the Settings plugin::

    app_config['oauth2'] = {
        'client_id': 'XXXXXXXXXXXXXXX.apps.googleusercontent.com',
        'client_secret': 'XXXXXXXXXXXXXXX',
        'developer_key': 'XXXXXXXXXXXXXXX'
    }

You can generate your own Client ID and Secret at the `Google API Console <https://code.google.com/apis/>`_.


Example
-------

.. module:: ferris.components.oauth

A quick example of how to use the OAuth2 features to interact with a Google API::

    from ferris import Controller
    from ferris.components import oauth
    from apiclient.discovery import build


    class Example(Controller):
        class Meta:
            components = (oauth.OAuth,)
            oauth_scopes = ['https://www.googleapis.com/auth/userinfo.profile']

        # Require credentials for the current user with the scopes listed above.
        @oauth.require_credentials
        def list(self):

            # Signed HTTP instance.
            http = self.oauth.http()

            # Access to Google's OAuth info API
            service = build('oauth2', 'v2', http=http)

            user_info = service.userinfo().get().execute()

            return "Hello, you are: %s" % user_info['name']

This example uses the Google user info service to create a friendly message. When you first navigate to `/example <http://localhost:8080>`_, you'll be taken through the entire OAuth Flow. You will only have to complete this once, as it stores the credentials for subsequence requests.


The OAuth Component
-------------------

.. autoclass:: OAuth

As in the example above, to use the component add it to the component list and be sure to specify the needed scopes using :attr:`Meta.oauth_scopes`::

    class Example(Controller):
        class Meta:
            components = (oauth.OAuth,)
            oauth_scopes = ['https://www.googleapis.com/auth/userinfo.profile']


You must decorate every action that needs credentials with either ``@require_credentials``, ``@provide_credentials``, or ``@require_admin_credentials``::


    @oauth.require_credentials
    def list(self):
        http = self.oauth.http()
        ...

    @route
    @oauth.provide_credentials
    def test(self):
        if not self.oauth.has_credentials():
            return "No credentials"
        ...

.. autofunction:: require_credentials

.. autofunction:: provide_credentials


Admin Credentials
-----------------

Sometimes you need to use one account for every user without using a service account. You can achieve this using *admin* credentials. These credentials can only be created by an administrator of an application but will be available for all requests to use.

.. autofunction:: require_admin_credentials

To create admin credentials use the OAuthManager plugin (enabled by default) located at `/admin/oauth_manager <http://localhost:8080/admin/oauth_manager>`.


Using Credentials
-----------------

Usually you'll want an ``http`` object that's signed with the credentials:

.. automethod:: OAuth.http

You can use this to build a service object::

    http = self.oauth.http()
    service = build('oauth2', 'v2', http=http)

Sometimes you want access to the credentials object directly:

.. automethod:: OAuth.credentials

To check if the credentials exist and are valid, you can use:

.. automethod:: OAuth.has_credentials

If you want to request that the user grant you access (if you're using provide instead of require) redirect them to the result of:

.. automethod:: OAuth.authorization_url


Finding Credentials
-------------------

Sometimes you need to use credentials outside of a controller context (such as in a task). You can find credentials using ``find_credentials``.

.. module:: ferris.core.oauth2
    :noindex:

.. autofunction:: find_credentials

Example::

    import httplib2
    from ferris.core.oauth2 import find_credentials

    creds = find_credentials(scopes=["https://www.googleapis.com/drive/file"], admin=True)
    http = httplib2.Http()
    creds.credentials.authorize(http)


The Oauth Manager
-----------------

.. module:: plugins.oauth_manager

The OAuth Manager plugin allows you to view, add, and delete credentials from the admin interface. Enable it in ``app/routes.py``::

    plugins.enable('oauth_manager')


Once enabled, you can access it via `/admin/oauth_manager <http://localhost:8080/admin/oauth_manager>`_.


OAuth2 Service Accounts
-----------------------

.. module:: ferris.core.oauth2
    :noindex:

The included OAuth2 Service Account plugin can help you when using Google's `Service Account Flow <https://developers.google.com/accounts/docs/OAuth2ServiceAccount>`_.

To use, configure the following :doc:`settings <settings>`::

    settings['oauth2_service_account'] = {
        'client_email': None,  # ..@developer.gserviceaccount.com
        'private_key' None,  # The private key in PEM format
        'developer_key': None
    }

By default, Google gives you the private key in pkcs12 (p12) format. For use with App Engine this key must be in PEM format. To convert from pkcs12 to PEM use this command on Linux / OS X::

    openssl pkcs12 -in key.p12 -out key.pem -nodes -nocerts

There are equivalent utilities for Windows.

Now you're ready to go.

.. autofunction:: build_credentials

Example::

    credentials = build_credentials(
        scope=["https://www.googleapis.com/drive/file"],
        user="user@domain.com")

    credentials.authorize(http)


.. warning:: Service account credentials will not work properly on local development environments that are missing the PyCrypto or PyOpenSSL modules. If you get errors concerning SignedJwtCredentials check your Python installation.

.. note:: Ferris ensures that the access token is stored in the datastore/memcache to reduce calls to Google's authorization service and avoid quota issues.
